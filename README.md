# flutter_jiandan

基于Google Flutter的高仿煎蛋客户端，支持Android和iOS。

![](./screenshots/ios01.jpeg)
![](./screenshots/ios02.jpeg)

## Android 扫码下载APK

![](./screenshots/apk-qrcode.png)

## Getting Started

For help getting started with Flutter, view our online
[documentation](https://flutter.io/).
